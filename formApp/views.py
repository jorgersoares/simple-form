from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import InsertFuncionario
from .models import Funcionario
#Imports reportlab pdf
from reportlab.pdfgen import canvas
import io
from django.http import FileResponse

def index(request):
    return render(request, 'formApp/index.html')


def cria(request):
    if request.method == "POST":
        form = InsertFuncionario(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    form = InsertFuncionario()
    return render(request, 'formApp/cadastrar.html', {'form': form})


def listar(request):
    funcionarios = Funcionario.objects.all().order_by('-nome')
    return render(request, 'formApp/listar.html', {'funcionarios': funcionarios})


def excluir(request, pk):
    funcionario = Funcionario.objects.get(id=pk)
    funcionario.delete()
    return redirect('listar')


def editar(request, pk):
    funcionario = Funcionario.objects.get(id=pk)
    form = InsertFuncionario(request.POST or None, instance=funcionario)
    if form.is_valid():
        form.save()
        return redirect('index')
    return render(request, 'formApp/editar.html', {'form': form})

def relatorio_pessoas(request):

    #Buffer para armezenar os dados que irão compor o pdf
    buffer = io.BytesIO()

    #Objeto pdf
    pdf = canvas.Canvas(buffer)
    pdf.setFont('Helvetica', 12)
    pdf.drawCentredString(300, 800, "Relação de Funcionários")

    #Dados do banco
    funcionarios = Funcionario.objects.all().order_by('-nome')
    
    #Desenha em tela os dados
    co_y = 780
    co_x = 40
    for funcionario in funcionarios:
        co_y -= 15
        pdf.drawString(co_x,co_y,"Nome: " + funcionario.nome + " Funcao: " + funcionario.funcao)
    #Finaliza a pagina
    pdf.showPage()
    #Salva o pdf
    pdf.save()
    #Seta o buffer para o inicio do arquivo
    buffer.seek(0)
    #Retorno como arquivo
    return FileResponse(buffer,as_attachment=True, filename='relatorio.pdf')